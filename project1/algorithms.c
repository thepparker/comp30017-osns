#include <stdlib.h>
#include <assert.h>

#include "algorithms.h"
#include "process.h"
#include "memory.h"
#include "utils.h"

void update_times(Scheduler *s)
{
    /* Increment the clock times on loaded processes (blocked/running) */

    MemoryChunk *smem;

    if (s->running != NULL)
    {
        s->running_quantum_remaining -= 1; /* decrement the quantum remaining on the current process */

        int proc_runtime = process_runtime(&s->running->proc);
        assert(proc_runtime != -1);

        //debug_printf("PID %d run period: %d, running time: %d, total running time: %d\n", s->running->proc.pid, proc_runtime, s->running->proc.runtime_completed[proc_runtime] + 1, s->running->proc.runtime[proc_runtime]);

        /* increment the completed runtime for this process */
        s->running->proc.runtime_completed[proc_runtime] += 1;
    }

    if (s->blocked != NULL)
    {
        ProcessList *b_proc_l = s->blocked;
        int proc_blocktime;

        while (b_proc_l != NULL)
        {
            proc_blocktime = process_blocktime(&b_proc_l->proc);
            assert(proc_blocktime != -1);

            /* increment the completed blocktime for this process */
            b_proc_l->proc.iotime_completed[proc_blocktime] += 1;

            b_proc_l = b_proc_l->next;
        }
    }

    smem = s->memory;

    /* just increment the time on all memory chunks, regardless of if they are free or not. this is needed so free_biggest_oldest() can work */
    while (smem != NULL)
    {
        smem->time += 1;
        smem = smem->next;
    }

    /* increment waiting times on queued processes */
    ProcessList *curr;
    for (int i = 0; i < MLFQ_LEVELS; i++)
    {
        if (s->runnable[i] != NULL)
        {
            curr = s->runnable[i];

            while (curr != NULL)
            {
                curr->proc.waiting_time += 1;

                curr = curr->next;
            }
        }
    }
}

void terminating_process(Scheduler *s)
{
    /* if the currently running process is ready to be terminated, we add it to the finished list. this method is invoked straight after update_times() */
    if (s->running != NULL)
    {
        int proc_last_run = s->running->proc.num_runtimes - 1;

        //debug_printf("PID %d has been running for %d on its last run\n", s->running->proc.pid, s->running->proc.runtime_completed[proc_last_run]);

        if (s->running->proc.runtime[proc_last_run] == s->running->proc.runtime_completed[proc_last_run])
        {
            /* the process has run for its max run time on the last run period. time to end! */
            //printf("time %d, %d terminating\n", s->clock_time, s->running->proc.pid);
            s->running->next = NULL; /* make sure the tail is NULL */
            /* add to finished list */
            if (s->finished == NULL)
            {
                s->finished = s->running; /* finished now contains only the running proc that has just ended */
            }
            else
            {
                /* loop to get to the end of the finished list */
                assert(add_node_to_list(s->finished, s->running) != false);
            }

            /* now we need to remove this process' memory chunk from the memory list */
            MemoryChunk *proc_mem;
            proc_mem = process_memory_chunk(s, s->running->proc.pid);
            assert(proc_mem != NULL);

            proc_mem->pid = free_mem; //flag the chunk as a free chunk

            /* perform calculations/actions for report */
            s->running->proc.time_completed = s->clock_time;
            s->running->proc.context_switches += 1;
            s->context_switches += s->running->proc.context_switches;

            s->running = NULL; /* now we just pop the scheduler's running proc, so something new can be added in the schedule loop */

            merge_memory_holes(s); /* try merge the newly created hole */
        }
    }
}

void start_new_processes(Scheduler* s)
{
    /*
    If a process has arrived (simulation has reached appropriate time), we need to move the process from the not_started list to the runnable list

    multiple processes can arrive at the same time, therefore we need to loop over the not_started list
    */
    if (s->not_started != NULL)
    {
        ProcessList *ns_proc_l = s->not_started, *tmp;

        while (ns_proc_l != NULL)
        {
            if (ns_proc_l->proc.time_created == s->clock_time)
            {
                /* this process is ready to be run, now we need to add it to the run queue. all new processes are added to the end of the highest priority queue (level 0) */
                if (s->runnable[0] == NULL)
                {
                    s->runnable[0] = ns_proc_l; /* set the first element of the run queue to this not-started process */

                    if (ns_proc_l == s->not_started)
                        s->not_started = s->not_started->next;

                    ns_proc_l = ns_proc_l->next;

                    s->runnable[0]->next = NULL; /* make sure the tail of the queue is NULL */

                    continue; /* jump to the next loop */
                }
                else
                {
                    /* loop to the end of the runnable queue */

                    //runnable_proc_l = s->runnable[0];

                    tmp = ns_proc_l;
                    ns_proc_l = ns_proc_l->next;

                    if (tmp == s->not_started)
                        s->not_started = s->not_started->next;

                    assert(add_node_to_list(s->runnable[0], tmp) != false);

                    continue;
                }
            }

            ns_proc_l = ns_proc_l->next;
        }
    }
}

void blocking_process(Scheduler* s)
{
    /*
    check if the currently running process is ready to block. if so, move it to the blocked list and put a new process in running

    this will mean that the process' current running time has FINISHED, and that it should have an iotime next

    if the process is still in the running list, it has not terminated (because terminate_process is run before this method).
    therefore the next action is to IO block if the appropriate running time has passed

    we will also check to see if there are any blocked processes that are now ready to be added to the queue
    */

    ProcessList *curr_blocked;

    if (s->blocked != NULL)
    {
        debug_printf("have process(es) blocked\n");
        ProcessList *tmp;

        curr_blocked = s->blocked;

        int new_queue_level;

        while (curr_blocked != NULL)
        {
            int blocktime = process_blocktime(&curr_blocked->proc);

            debug_printf("Checking PID %d. blocktime: %d, total blocking time: %d, time blocked: %d\n", curr_blocked->proc.pid, blocktime, curr_blocked->proc.iotime[blocktime], curr_blocked->proc.iotime_completed[blocktime]);

            if (blocktime >= (int)curr_blocked->proc.num_iotimes)
                return; //if for some reason we're beyond the number of iotimes, escape!

            if (curr_blocked->proc.iotime[blocktime] == curr_blocked->proc.iotime_completed[blocktime])
            {
                /* this process has completed its IO time. time to add it back to the queue */
                new_queue_level = ((curr_blocked->proc.last_queue_level == 0) ? 0 : (curr_blocked->proc.last_queue_level - 1));

                curr_blocked->proc.current_block += 1; //increment the current block, which will be used for the next block (if there is one)

                debug_printf("PID %d is ready to be added back to queue level %d. curr_blocked =  %d\n", curr_blocked->proc.pid, new_queue_level, curr_blocked);

                tmp = curr_blocked;
                curr_blocked = curr_blocked->next;

                if (s->blocked == tmp)
                    s->blocked = s->blocked->next;
                else
                    assert(remove_from_list(s->blocked, tmp) != false); /* remove the node from the blocked list */

                if (s->runnable[new_queue_level] == NULL)
                {
                    s->runnable[new_queue_level] = tmp;
                    s->runnable[new_queue_level]->next = NULL; //NULL the tail

                    continue;
                }
                else
                {
                    /* loop to the end of the runnable queue */
                    //runnable_proc_l = s->runnable[new_queue_level];

                    assert(add_node_to_list(s->runnable[new_queue_level], tmp) != false);  /* add tmp to s->runnable */

                    continue;
                }
            }
            curr_blocked = curr_blocked->next;
        }
    }

    if (s->running != NULL)
    {
        /* we have a running process that was not terminated */
        int runtime = process_runtime(&s->running->proc);
        assert(runtime != -1);

        if (s->running->proc.runtime[runtime] == s->running->proc.runtime_completed[runtime])
        {
            /* running time has elapsed, now we should move it to the end of the blocked list */
            s->running->proc.current_run += 1; /* increment the run var, which will be used for the next run */

            if (s->blocked == NULL)
            {
                s->blocked = s->running;
            }
            else
            {
                assert(add_node_to_list(s->blocked, s->running) != false);
            }

            //s->running->proc.runtime_completed[runtime] = s->clock_time; /* set when this run completed */

            debug_printf("moved pid %d to blocked list. runtime completed at %d\n", s->running->proc.pid, s->running->proc.runtime_completed[runtime]);

            s->running->proc.context_switches += 1;
            s->running = NULL; /* there is no longer process running */
        }
    }
}

void preempt(Scheduler* s, Algorithm alg)
{
    /*
    this method is only used for mlfq_pre, and is invoked AFTER BLOCKING, TERMINATING, NEW PROCESSES AND QUANTUM END

    we check if there is a process in a queue that is higher priority than the currently running process. if there is one, we move the running process to the end of its original queue so the higher one can be run
    */
    if (alg != mlfq_pre)
        return;

    if (s->running != NULL)
    {
        /* only need to worry about queues that are higher than the currently running process */

        int current_queue_level = s->running_queue_level;

        for (int i = 0; i < current_queue_level; i++)
        {
            debug_printf("Running queue level: %d. Checking queue %d for procs\n", current_queue_level, i);
            if (s->runnable[i] != NULL)
            {
                /* there is a process with a higher priority than the one currently running, so move the running process to the back of zee queue */
                debug_printf("PID %d pre-empts PID %d. Running queue level: %d\n", s->runnable[i]->proc.pid, s->running->proc.pid, current_queue_level);

                if (s->runnable[current_queue_level] == NULL)
                {
                    s->running->next = NULL;
                    s->runnable[current_queue_level] = s->running;
                }
                else
                {
                    //ProcessList *runnable_proc_l = s->runnable[current_queue_level];
                    debug_printf("adding PID %d to end of queue %d\n", s->running->proc.pid, current_queue_level);

                    assert(add_node_to_list(s->runnable[current_queue_level], s->running) != false);
                }

                s->running->proc.context_switches += 1;

                s->running = NULL; /* remove from running */

                break;
            }
        }
    }
}

void end_quantum(Scheduler *s, Algorithm alg)
{
    /*
    this method is invoked AFTER BLOCKING, TERMINATING, AND NEW PROCESSES are checked

    check if the process has expired the quantum for its current queue level and the algorithm being used
    if so, move it from running to the next queue level.

    QUEUE LEVELS:

        mlfq_np:
        Q1 = 4
        Q2 = 8
        Q3 = 16

        mlfq_preempt:
        Q1 = 4
        Q2 = 8
        Q3 = 32
    */

    if (s->running != NULL)
    {
        unsigned int current_quantum;

        if (alg == mlfq_np)
            current_quantum = mlfq_np_quanta[s->running_queue_level];
        else if (alg == mlfq_pre)
            current_quantum = mlfq_pre_quanta[s->running_queue_level];
        else if (alg == rr)
            current_quantum = rr_quantum;
        else
            current_quantum = fcfs_quantum; /* the only remaining possibility */

        if (s->running_quantum_remaining == 0) /* the process has been running for the quantum specified for its queue level */
        {
            /* processes are only bumped to the next level if the algorith is a MLFQ. else, it's just added to the end of the 0 queue */
            int new_queue_level;
            if (alg == mlfq_np || alg == mlfq_pre)
                new_queue_level = ((s->running_queue_level < 2) ? (s->running_queue_level + 1) : 2);
            else
                new_queue_level = 0;

            debug_printf("PID %d has expired its quantum. Moving to queue level %d\n", s->running->proc.pid, new_queue_level);

            if (s->runnable[new_queue_level] == NULL)
            {
                s->runnable[new_queue_level] = s->running;
            }
            else
            {
                //ProcessList *runnable_proc_l = s->runnable[new_queue_level];
                assert(add_node_to_list(s->runnable[new_queue_level], s->running) != false);
            }

            s->running->proc.context_switches += 1;

            s->running = NULL;
        }
    }
}

bool schedule(Scheduler *s, Algorithm alg)
{
    /*
        Move ready processes to running based on queue position and algorithm

        @return false when a process is already running or there are no process to run, true if a new process is scheduled by this method
    */
    if (s->running != NULL)
    {
        //debug_printf("process still running during scheduling\n");
        return false;
    }

    int num_queue_levels = 1;

    if (alg == mlfq_np || alg == mlfq_pre)
        num_queue_levels = MLFQ_LEVELS;

    for (int i = 0; i < num_queue_levels; i++)
    {
        //debug_printf("checking queue %d\n", i);
        if (s->runnable[i] != NULL)
        {
            debug_printf("process in queue %d\n", i);
            /* we only worry about the highest queue level available */
            ProcessList *r_proc_l = s->runnable[i]; /* we want the first process in the list */

            /* move the queue list head to the next element if possible, else the list is now empty and should be NULL */
            if (r_proc_l->next != NULL)
                s->runnable[i] = r_proc_l->next;
            else
                s->runnable[i] = NULL;

            s->running = r_proc_l;
            s->running->next = NULL; /* make sure the tail is NULL */

            int new_quantum;
            /* get the quantum for the new process */
            if (alg == mlfq_np)
                new_quantum = mlfq_np_quanta[i];
            else if (alg == mlfq_pre)
                new_quantum = mlfq_pre_quanta[i];
            else if (alg == rr)
                new_quantum = rr_quantum;
            else
                new_quantum = fcfs_quantum; /* the only remaining possibility */

            /* set the scheduler variables */
            s->running_quantum_remaining = new_quantum;
            s->running_queue_level = i;
            s->running->proc.last_queue_level = i;

            /* statistic display */
            s->running->proc.context_switches += 1;
            if (s->running->proc.response_time == -1)
                s->running->proc.response_time = s->clock_time - s->running->proc.time_created;

            debug_printf("PID: %d scheduled\n", r_proc_l->proc.pid);
            return true; /* we've scheduled a new process, so the return is TRUE */
        }
    }

    return false; /* no processes are ready to be run */
}

int process_runtime(Process *proc)
{
    /*if (proc->num_runtimes == 1)
    {
        return 0;
    }

    /*
    loop over the run times to find the process' current run period. if runtime < completed runtime at an index, then that index is the current run.
    however, if runtime == completed runtime, we check to see if the next index is 0, which means that the process has completed this run period, but not started another

    for (unsigned int i = 0; i < proc->num_runtimes; i++)
    {
        if (proc->runtime_completed[i] < proc->runtime[i])
        {
            return (int)i;
        }
        else if (proc->runtime_completed[i] == proc->runtime[i])
        {
            if (proc->runtime_completed[i+1] == 0)
                return (int)i;
        }
    }

    return -1;
    */

    return proc->current_run;
}

int process_blocktime(Process *proc)
{
    /* see above for logic */
    /*
    if (proc->num_iotimes == 1)
    {
        return 0;
    }

    for (unsigned int i = 0; i < proc->num_iotimes; i++)
    {
        if (proc->iotime_completed[i] < proc->iotime[i])
        {
            return (int)i;
        }
        else if (proc->iotime_completed[i] == proc->iotime[i])
        {
            if (proc->iotime_completed[i+1] == 0)
                return (int)i;
        }
    }

    return -1;
    */

    return proc->current_block;
}

MemoryChunk *process_memory_chunk(Scheduler *s, int pid)
{
    MemoryChunk *smem;
    smem = s->memory;

    while (smem != NULL)
    {
        if (smem->pid == pid)
        {
            return smem;
        }

        smem = smem->next;
    }

    return NULL;
}

bool add_node_to_list(ProcessList *list, ProcessList *node)
{
    if (node == NULL || list == node || list == NULL)
        return false;

    /* takes a node and adds it to the tail of the specified list */
    ProcessList *curr;

    curr = list; /* set the current pointer to the head of the list (or atleast, the head given to us) */

    while (true)
    {
        if (curr->next == NULL)
        {
            curr->next = node;
            node->next = NULL;

            return true;
        }
        curr = curr->next;
    }

    return false;
}

bool remove_from_list(ProcessList *list, ProcessList *node)
{
    if (node == NULL || list == node || list == NULL)
        return false;

    ProcessList *tmp, *curr;

    curr = list;

    while (true)
    {
        if (curr == node)
        {
            tmp->next = curr->next; //tmp->next points to CURR, so we skip over it. node is now not associated with this list
            node->next = NULL; //node is now a floating node, with nothing pointing to it and nothing being pointed to by it

            return true;
        }
        tmp = curr;
        curr = curr->next;
    }

    return false;
}
