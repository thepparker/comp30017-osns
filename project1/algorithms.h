#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <stdbool.h>

#include "process.h"

void update_times(Scheduler *s);
void terminating_process(Scheduler *s);
void start_new_processes(Scheduler* s);
void blocking_process(Scheduler* s);
void preempt(Scheduler* s, Algorithm alg);
void end_quantum(Scheduler *s, Algorithm alg);
bool schedule(Scheduler *s, Algorithm alg);

int process_runtime(Process *proc); /* gets the current run time of a process */
int process_blocktime(Process *proc); /* as above, but current io time */

MemoryChunk *process_memory_chunk(Scheduler *s, int pid); /* gets the process' memory chunk */
bool add_node_to_list(ProcessList *list, ProcessList *node); /* add node to a list */
bool remove_from_list(ProcessList *list, ProcessList *node); /* remove node from a list */

#endif
/*ALGORITHMS_H*/
