#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "memory.h"
#include "process.h"
#include "utils.h"

bool first_fit(Scheduler *s)
{
    /*
    Find the first suitable space in memory

    This is called after a new process has been moved from the ready queue to 'running', and needs to be allocated memory.

    first we check if there's a chunk of memory large enough to hold the process, then we allocate a chunk of that free memory to the process.
    sometimes the free memory may be exactly the amount of memory required, other times there may be extra. the remaining memory is put on the tail end of
    the new chunk, and added to the list

    if no memory hole is large enough, return false so free_biggest_oldest() can be called
    */

    //debug_printf("doing first fit!\n");

    MemoryChunk *sched_mem = s->memory;
    int mem_remaining;
    while (sched_mem != NULL)
    {
        if (sched_mem->pid == free_mem)
        {
            mem_remaining = sched_mem->size - s->running->proc.mem_size;
            if (mem_remaining >= 0)
            {
                /* there is enough free space! */

                sched_mem->pid = s->running->proc.pid;
                sched_mem->size = s->running->proc.mem_size;
                sched_mem->time = 0;

                if (mem_remaining > 0)
                {
                    /* we have enough memory for the process, but there's some left over. therefore, we need to allocate a new memory chunk for the gap */
                    MemoryChunk *mem_gap = malloc_wrapper(sizeof(MemoryChunk));
                    mem_gap->pid = free_mem;
                    mem_gap->size = mem_remaining;
                    mem_gap->time = 0;

                    /*
                    now that we've allocated and set the values for the new chunk, we change the pointers so that the list remains intact, keeping in mind that the memory is
                    allocated in a descending order, so that used chunks come before free memory
                    */
                    mem_gap->next = sched_mem->next;
                    sched_mem->next = mem_gap;
                }

                return true; /* break out of the loop and return true because we've allocated memory */
            }
        }
        sched_mem = sched_mem->next;
    }
    return false;
}

void free_biggest_oldest(Scheduler *s)
{
    /*
    This method is called when first_fit() cannot assign memory for a process.
    we check all the processes loaded, and 'free' (set the pid to free_mem) the memory of whichever process is using the most memory, or if two are equal, which ever process has been loaded the longest
    then we call merge_memory_holes(), so that first fit can be run again to find a suitable gap
    */
    debug_printf("free_biggest_oldest!\n");
    MemoryChunk *sched_mem = s->memory;

    MemoryChunk *chunk_to_free = NULL;

    unsigned int large_mem_size = 0, large_time = 0;

    while (sched_mem != NULL)
    {
        if (sched_mem->pid != free_mem)
        {
            debug_printf("possible pid to free: %d, size: %d, time: %d\n", sched_mem->pid, sched_mem->size, sched_mem->time);
            if (sched_mem->size > large_mem_size)
            {
                large_mem_size = sched_mem->size;
                large_time = sched_mem->time;

                chunk_to_free = sched_mem;
            }
            else if (sched_mem->size == large_mem_size && sched_mem->time > large_time)
            {
                large_time = sched_mem->time;
                chunk_to_free = sched_mem;
            }

        }

        sched_mem = sched_mem->next;
    }

    debug_printf("pid: %d chunk_to_free = %d, sched_mem = %d, time = %d, size = %d\n", -1, chunk_to_free, sched_mem, large_time, large_mem_size);
    //if (chunk_to_free != NULL) //sometimes we need two merges in a row, which means that chunk_to_free will be NULL
    chunk_to_free->pid = free_mem;

    merge_memory_holes(s); /* check to see if we can merge the newly created hole */
}

void merge_memory_holes(Scheduler *s)
{
    /* this method will loop over memory chunks in the scheduler's memory list and merge all adjacent free chunks, until there are no more chunks to be merged */
    MemoryChunk *sched_mem = s->memory;
    bool merged;

    while (sched_mem != NULL)
    {
        merged = false;
        if (sched_mem->next != NULL)
        {
            if (sched_mem->pid == free_mem && sched_mem->next->pid == free_mem)
            {
                /* these two holes are adjacent. merge them by adding the sizes together, and removing the rightmost one from the list */
                MemoryChunk *tmp = sched_mem->next;
                sched_mem->size += tmp->size;
                sched_mem->next = tmp->next; //skip over the tmp struct

                free_wrapper(tmp); //free the memory allocated to this struct

                merged = true;
            }
        }

        if (merged)
            sched_mem = s->memory; //go back to the start, so we can loop until all slots are merged
        else
            sched_mem = sched_mem->next;
    }
}

unsigned int memory_in_use(Scheduler *s)
{
    /* Returns the amount of memory in use by the scheduler's processes (does not include free chunks) */

    MemoryChunk *sched_mem;
    unsigned int mem_usage = 0;

    sched_mem = s->memory;

    while (sched_mem != NULL)
    {
        if (sched_mem->pid != free_mem)
            mem_usage += sched_mem->size;

        sched_mem = sched_mem->next;
    }

    return mem_usage;
}

unsigned int memory_size(Scheduler *s)
{
    /* Returns the total amount of memory in use, including free chunks */
    MemoryChunk *sched_mem = s->memory;
    unsigned int mem_size = 0;

    while (sched_mem != NULL)
    {
        mem_size += sched_mem->size;

        sched_mem = sched_mem->next;
    }

    return mem_size;
}

unsigned int processes_in_memory(Scheduler *s)
{
    /* Returns the number of processes in memory (PIDs in memory list != 'free_mem') */
    MemoryChunk *sched_mem;

    unsigned int num_proc = 0;

    sched_mem = s->memory; /* get the memorychunk of the first active process. s->memory is the scheduler's memorychunk */
    while (sched_mem != NULL)
    {
        if (sched_mem->pid != free_mem)
            num_proc += 1;

        sched_mem = sched_mem->next;
    }

    return num_proc;
}

unsigned int holes_in_memory(Scheduler *s)
{
    /* Returns how many holes are in the memory (nodes in the memory list that have PID 'free_mem', which are considered empty memory holes) */

    MemoryChunk *sched_mem = s->memory;
    unsigned int num_holes = 0;

    while (sched_mem != NULL)
    {
        if (sched_mem->pid == free_mem)
            num_holes += 1;

        sched_mem = sched_mem->next;
    }

    return num_holes;
}

bool memory_empty(Scheduler *s)
{
    /* Returns whether the memory is empty or not */

    if (memory_in_use(s) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool is_in_memory(Scheduler *s, int pid)
{
    MemoryChunk *sched_mem;
    sched_mem = s->memory;

    while (sched_mem != NULL)
    {
        if (sched_mem->pid == pid)
        {
            return true;
        }
        sched_mem = sched_mem->next;
    }

    return false;
}
