#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "logging.h"

#define MAX_PATH_LEN 1024

static char log_file[MAX_PATH_LEN];
static int log_in_use;
/*
* This function is to be called once,
* before the first call to log_write is made.
*
* log_file_name is the path and file name of the file to be used for logging.
*/
void log_setUp(char* log_file_name)
{
    strcpy(log_file, log_file_name); /* copy log file to our global var */
    printf("Logs will be written to %s\n", log_file);

    log_in_use = 0; /* used to control mutual exclusion between client threads */
}

/*
* Writes a log entry to the log file.
* entries must be formatted with this pattern: "%s: %s: %s: %s: %s\n"
* From left to right the %s are to be:
*  - a time stamp, in local time.
*  - module (which part of the program created the entry?)
*  - p1
*  - p2
*  - p3
*
* Note: This function may be called from multiple threads and hence must
*       ensure that only one thread is writing to the file at any given time.
*/
void log_write(char* module, char* p1, char* p2, char* p3)
{
    /* just sleep wait if log is in use. writes shouldn't take more than a second */
    while (log_in_use)
    {
        sleep(1);
    }

    log_in_use = 1;

    FILE *fileptr = NULL;
    fileptr = fopen(log_file, "a");
    if (fileptr == NULL)
    {
        printf("ERROR: Unable to open %s for writing\n", log_file);
    }
    else
    {
        /* get our time stamp using thread-safe functions */
        time_t time_values;
        time_values = time(NULL);

        struct tm localtime_result;
        char timestamp[32];
        localtime_r(&time_values, &localtime_result);
        /* Fri May 17 11:54:38 2013: Main: POP3 Server Started: :  */
        strftime(timestamp, sizeof(timestamp), "%a %b %d %H:%M:%S %Y", &localtime_result);
        if (p3 == NULL)
            fprintf(fileptr, "%s: %s: %s: %s: \n", timestamp, module, p1, p2);
        else
            fprintf(fileptr, "%s: %s: %s: %s: %s\n", timestamp, module, p1, p2, p3);

        fclose(fileptr);
    }

    log_in_use = 0; /* clear the mutex */
}

