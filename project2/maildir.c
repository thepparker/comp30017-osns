#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "maildir.h"
#include "logging.h"

#define maxUserLen 50
#define maxPassLen 20

#define MAX_PATH_LEN 1024

static char root_dir[MAX_PATH_LEN]; /* dirty global (within maildir.c) for holding the path of the mail directories */
/*
* This function is to be called once at the beginning
* to set the root path of the maildirs.
*/
void dir_set_path(char* path)
{
    //root_dir = malloc(MAX_PATH_LEN);
    memset(root_dir, 0, MAX_PATH_LEN);
    char *real_dir = realpath(path, root_dir); /* get the full path and store it in root_dir */

    if (real_dir == NULL)
    {
        printf("NOTE: Unable to resolve path for MailDir %s\n", path);
    }

    /* append trailing / to working dir */
    strcat(root_dir, "/");
    printf("Working mail dir: %s\n", root_dir);

}

/*
* This function checks the given username and password
* against the stored values.
*
* If the username is valid (has a corresponding folder)
* and the given password matches the pass file
* then the function should return 'true'.
*
* Otherwise it is to return 'false'.
*
* Note 'true' and 'false' are defined in maildir.h
*/
bool check_user(char* username, char* password)
{
    log_write("MailDir", username, "check_user", "");
    /* find the pass file, open it, and check if the passwords match */
    printf("User %s attempting to authenticate with password %s\n", username, password);
    bool authed = false;

    /* construct path to user's mail directory and the file */
    char user_filepath[MAX_PATH_LEN];

    strcpy(user_filepath, root_dir); /* copy the root dir to our temp var. the maildir will always have a trailing / */
    strncat(user_filepath, username, maxUserLen); /* append the user */
    strcat(user_filepath, "/"); /* add a separating slash */
    strcat(user_filepath, "pass"); /* append the file name */

    /* now we can perform operations on the file */
    FILE *fileptr = fopen(user_filepath, "r");
    if (fileptr == NULL)
    {
        printf("ERROR: Unable to open file %s\n", user_filepath);
        return authed;
    }

    char user_pass[maxPassLen+1];
    if (fgets(user_pass, maxPassLen+1, fileptr) == NULL)
    {
        printf("Error reading password file, or password file is empty\n");
        fclose(fileptr);
        return authed;
    }

    //printf("\tData read: %s\n", user_pass);
    fclose(fileptr);

    /* strip newline or CRLF from the password, so it can be compared properly */
    size_t passlen = strlen(user_pass);
    if (user_pass[passlen-1] == '\n')
        user_pass[passlen-1] = '\0';
    if (user_pass[passlen-2] == '\r')
        user_pass[passlen-2] = '\0';

    //printf("Read password \"%s\" for \"%s\"\n", user_pass, username);
    if (strcmp(user_pass, password) == 0)
    {
        /* password is OK! */
        printf("\tUser %s successfully authenticated\n", username);
        authed = true;
    }
    else
    {
        printf("\t%s specified invalid password \"%s\". Correct password: \"%s\". Rejected\n", username, password, user_pass);
    }

    return authed;
}

/*
* Constructs a FilesStruct as described in maildir.h
* for the maildir of the givne user.
*/
FilesStruct* dir_get_list(char* user)
{
    log_write("MailDir", user, "GetList", "");

    FilesStruct *tmp_struct = malloc(sizeof(FilesStruct));
    tmp_struct->count = 0;

    tmp_struct->FileNames = NULL;
    tmp_struct->FileSize = NULL;

    int *file_sizes = NULL;
    char **string_array = NULL;

    void *tmp_alloc = NULL;

    /* construct user dir */
    char user_maildir[MAX_PATH_LEN];

    strcpy(user_maildir, root_dir); /* copy the root dir to our temp var. the root dir will always have a trailing / */
    strncat(user_maildir, user, maxUserLen); /* append the user */
    printf("MailDir for user %s: %s\n", user, user_maildir);

    /* open the directory (if it exists), and loop over the contents */
    DIR *dirptr = NULL;

    struct dirent *directory_ent;

    if ((dirptr = opendir(user_maildir)) == NULL)
    {
        printf("ERROR: Unable to open user directory for %s\n", user);
        return NULL;
    }

    struct stat filestat;
    char filepath[MAX_PATH_LEN];

    int error = 0;

    while ((directory_ent = readdir(dirptr)) != NULL)
    {
        snprintf(filepath, MAX_PATH_LEN, "%s/%s", user_maildir, directory_ent->d_name);
        //printf("\tFound file %s\n", filepath);
        /* if the file cannot be read, skip! */
        if (stat(filepath, &filestat) == -1)
        {
            printf("ERROR: Cannot stat file %s\n", filepath);
            continue;
        }

        /* skip folders (such as '.' and '..') and the pass file */
        if (S_ISDIR(filestat.st_mode) || (strcmp(directory_ent->d_name, "pass") == 0)) continue; 
        //printf("\tFile is not a folder or the password file\n");

        /* construct our FilesStruct shit... o gawd */

        tmp_alloc = realloc(file_sizes, sizeof(int) * (tmp_struct->count + 1));
        if (tmp_alloc == NULL)
        {
            //BAIL!
            printf("ERROR: Unable to allocate memory for file sizes\n");

            error = 1;
            break;
        }
        file_sizes = (int *)tmp_alloc;

        file_sizes[tmp_struct->count] = filestat.st_size; /* store the file size */

        tmp_alloc = realloc(string_array, sizeof(char *) * (tmp_struct->count + 1));
        if (tmp_alloc == NULL)
        {
            //BAIL!
            printf("ERROR: Unable to allocate memory for file name array\n");

            error = 1;
            break;
        }
        string_array = (char **)tmp_alloc;

        tmp_alloc = malloc(sizeof(char) * (strlen(directory_ent->d_name) + 1));
        if (tmp_alloc == NULL)
        {
            printf("ERROR: Unable to allocate memory for file name\n");

            error = 1;
            break;
        }

        string_array[tmp_struct->count] = (char *)tmp_alloc;
        strcpy(string_array[tmp_struct->count], directory_ent->d_name);

        /*printf("\tAdded %s at index %d in string_array (array val: %s). Size: %d bytes\n", 
                (char *)tmp_alloc, tmp_struct->count, string_array[tmp_struct->count], file_sizes[tmp_struct->count]);*/

        tmp_struct->count += 1;
    }

    printf("\tFinished searching %s's mail directory. Num files: %d, error: %d\n", user, tmp_struct->count, error);

    if (dirptr != NULL)
        closedir(dirptr);

    if (directory_ent != NULL)
        free(directory_ent);

    if (error > 0)
    {
        if (file_sizes != NULL)
            free(file_sizes);

        if (string_array != NULL)
        {
            for (int i = 0; i < tmp_struct->count; i++)
            {
                free((void *)string_array[i]);
            }

            free(string_array);
        }
        free(tmp_struct);

        return NULL;
    }
    else
    {
        tmp_struct->FileNames = string_array;
        tmp_struct->FileSize = file_sizes;

        return tmp_struct;
    }
}

/*
* Delete the given file from the maildir of the given user.
*/
void delete_mail(char* user, char* filename)
{
    log_write("MailDir", user, "Delete", filename);
    /* construct path to user's mail directory and the file */
    char user_filepath[MAX_PATH_LEN];

    strcpy(user_filepath, root_dir); /* copy the root dir to our temp var. the maildir will always have a trailing / */
    strncat(user_filepath, user, maxUserLen); /* append the user */
    strncat(user_filepath, "/", 1); /* add a separating slash */
    strncat(user_filepath, filename, strlen(filename)); /* append the file name */

    printf("delete_mail: filepath is %s\n", user_filepath);

    if (remove(user_filepath) == 0)
        printf("\tSuccessfully removed %s\n", user_filepath);
    else
        printf("\terror removing %s\n", user_filepath);
}

/*
* Returns the contents of a given file from a user's maildir.
*
* This function MUST also byte-stuff the termination character
* as described in the project spec.
*
* This function MUST also append the termination character ('.') and a CRLF pair
* to the end.
*/
char* get_file(char* user, char* filename)
{
    log_write("MailDir", user, "GetFile", filename);
    if ((strlen(filename) + strlen(root_dir)) > MAX_PATH_LEN)
    {
        printf("ERROR: filename and root directory are too long\n");
        return NULL;
    }

    printf("Getting data in file %s\n", filename);

    /* construct path to user's mail directory and the file */
    char user_filepath[MAX_PATH_LEN];

    strcpy(user_filepath, root_dir); /* copy the root dir to our temp var. the maildir will always have a trailing / */
    strncat(user_filepath, user, maxUserLen); /* append the user */
    strncat(user_filepath, "/", 2); /* add a separating slash */
    strncat(user_filepath, filename, strlen(filename)); /* append the file name */

    printf("get_file: filepath is %s\n", user_filepath);
    /* now we can perform operations on the file */

    FILE *fileptr = fopen(user_filepath, "r");
    if (fileptr == NULL)
    {
        printf("ERROR: Unable to open file %s\n", user_filepath);
        return NULL;
    }
    printf("\tfile opened for reading\n");

    int line_size = 256, malloc_len, first_line = 1; /* vars used for memory allocation and initialisation */

    char *linebuffer = malloc(sizeof(char) * line_size); /* buffer for line read from file */
    if (linebuffer == NULL)
    {
        printf("ERROR: Unable to alloc memory for line buffer\n");
        fclose(fileptr);
        return NULL;
    }
    memset(linebuffer, 0, sizeof(char) * line_size);

    char *fgets_data = malloc(sizeof(char) * 256); /* read 256 chars at most at once */
    if (fgets_data == NULL)
    {
        free(linebuffer);
        fclose(fileptr);
        return NULL;
    }

    char *fullbuffer = NULL;
    void *tmp_alloc;

    /* read each line into linebuffer. fgets returns NULL on error and EOF, and reads up to \n */
    while (fgets(fgets_data, 256, fileptr) != NULL)
    {
        //printf("data read: %s\n", fgets_data);
        if (line_size < (int)(strlen(linebuffer) + strlen(fgets_data) + 1))
        {
            /* need to allocate more memory to the line buffer */
            line_size += strlen(fgets_data) + 1;
            tmp_alloc = realloc(linebuffer, line_size);
            if (tmp_alloc == NULL)
            {
                printf("ERROR: Unable to alloc memory for line buffer\n");
                fclose(fileptr);
                free(linebuffer);
                free(fgets_data);
                return NULL;
            }
            linebuffer = (char *)tmp_alloc;
            tmp_alloc = NULL;

            printf("Line size increased. new size: %d\n", line_size);
        }

        /* append the data read to the line buffer */
        strcat(linebuffer, fgets_data);

        if (linebuffer[strlen(linebuffer)-1] != '\n') /* check if we have a full line */
        {
            /* we need to read more data */

            continue;
        }
        /* else, we have a full line. YAY! */

        /* calculate how much memory is needed for the full buffer */
        malloc_len = strlen(linebuffer) * sizeof(char); /* bytes read from file */

        if (linebuffer[0] == '.')
        {
            malloc_len += sizeof(char); /* need another byte for byte-stuffing '.' */
        }
        if (fullbuffer != NULL)
        {
            //printf("size full buffer: %d, fullbuffer null term: %d\n", strlen(fullbuffer)+1, fullbuffer[strlen(fullbuffer) + 1]);
            malloc_len += strlen(fullbuffer) * sizeof(char); /* bytes for the existing length of the buffer */
        }
        malloc_len += sizeof(char); /* byte for null terminator */

        /*printf("Next size of fullbuffer mem: %d. length read: %d, first char of line read: %d, last char: %d\n", 
                    malloc_len, strlen(linebuffer), linebuffer[0], linebuffer[strlen(linebuffer)]);*/

        tmp_alloc = realloc(fullbuffer, malloc_len);

        if (tmp_alloc == NULL)
        {
            printf("ERROR: Unable to allocate memory in file reading. ABORT!\n");

            if (linebuffer != NULL)
                free(linebuffer);
            if (fullbuffer != NULL)
                free(fullbuffer);

            fclose(fileptr);
            return NULL;
        }

        fullbuffer = (char *)tmp_alloc;

        /* if this is the very first time fullbuffer is being written to, we need to initialise it.
        if we don't, we're going to be concatenating with an an unitialised string resulting in
        undefined behavior. yes, i wasted about 12 hours debugging this shit
        */

        if (first_line)
        {
            /* zero the fullbuffer memmory */
            memset(fullbuffer, 0, malloc_len);
            first_line = 0;
        }

        /* if the first char of the line is a '.', we need to stuff it by adding another '.' in front
        to do this, we append a '.' to the fullbuffer before we append the line that was read */
        if (linebuffer[0] == '.')
        {
            /* append .line */
            strcat(fullbuffer, ".");
            strcat(fullbuffer, linebuffer);
        }
        else
        {
            /* just append the line */
            strcat(fullbuffer, linebuffer);
        }

        /* empty the line buffer */
        memset(linebuffer, 0, sizeof(char) * line_size);
    }
    if (linebuffer != NULL)
        free(linebuffer);

    if (fgets_data != NULL)
        free(fgets_data);

    fclose(fileptr);

    /* make sure something was actually read first */
    if (fullbuffer == NULL)
    {
        printf("\tNo data was read from the file... wat?\n");
        return NULL;
    }

    /* append the muli-line terminator CRLF.CRLF to the end
    \r\n.\r\n\0*/
    tmp_alloc = realloc(fullbuffer, sizeof(char) * (strlen(fullbuffer) + 6));
    if (tmp_alloc == NULL)
    {
        printf("ERROR: Unable to allocate memory for fullbuffer\n");
        free(fullbuffer);
        return NULL;
    }

    fullbuffer = (char *)tmp_alloc;
    strcat(fullbuffer, "\r\n.\r\n");

	return fullbuffer;
}

