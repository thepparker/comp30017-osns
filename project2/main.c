#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>

#include "socket.h"
#include "maildir.h"
#include "logging.h"
#include "pop.h"

#define MAX_CONCURRENT_CONNECTIONS 100

/* a struct to hold client data necessary to pass to the client's thread */
struct client_thread_data
{
    int clientfd, thread_id;

    pthread_t *server_threads;
};

void *serve_client_thread(void *thread_data); /* thread entry point */
void signal_handler(int signum); /* handler for signals such as SIGINT and SIGTERM */
void cleanup_threads(); /* cleanup function for when the program must exit */
void thread_cleanup_handler(void *thread_data); /* a handler that the thread pops on end */

/* an array to store all the client threads. global for signal handler */
static pthread_t client_threads[MAX_CONCURRENT_CONNECTIONS];
int cleanup = 0;
int serverfd;

int main(int argc, char **argv)
{
	if (argc != 4) {
		printf("USAGE: %s [port] [maildir path] [log file]\n", argv[0]);
		printf("NOTE: the maildir path must end with an '/'\n");
		exit(1);
	}

    struct sigaction signal_action;
    signal_action.sa_handler = signal_handler;
    signal_action.sa_flags = 0;
    sigemptyset(&signal_action.sa_mask);

    if (sigaction(SIGTERM, &signal_action, NULL) == -1)
    {
        printf("ERROR: Unable to setup SIGTERM handler\n");
        exit(EXIT_FAILURE);
    }

    if (sigaction(SIGINT, &signal_action, NULL) == -1)
    {
        printf("ERROR: Unable to setup SIGINT handler\n");
        exit(EXIT_FAILURE);
    }

    int portno, new_clientfd;

    /* get command line params */
    portno = atoi(argv[1]);
    char *mail_dir = argv[2], *log_dir = argv[3];

    /* setup logging and mail root */
    dir_set_path(mail_dir);
    log_setUp(log_dir);

    /* setup listen socket */
    serverfd = socket_setup(portno);
    log_write("Main", "POP3 Server Started", "", "");

    /* initialise client thread array */
    for (int i = 0; i < MAX_CONCURRENT_CONNECTIONS; i++)
    {
        client_threads[i] = 0;
    }

    while ((new_clientfd = socket_get_new_connection(serverfd)) != -1)
    {
        log_write("Main", "New Connection", "", "");

        //pop_protocol(new_clientfd); /* serve the client the pop protocol */

        /* find the first null thread ID */
        int tid = -1, t_status;
        for (int i = 0; i < MAX_CONCURRENT_CONNECTIONS; i++)
        {
            if (client_threads[i] == 0)
            {
                tid = i;
                break;
            }
        }

        if (tid == -1)
        {
            printf("WARNING: Server is at maximum capacity\n");
            log_write("Main", "Maximum capacity", "Rejecting connection", "");
            close_connection(new_clientfd);
            continue;
        }

        /* allocate the client thread data struct */
        struct client_thread_data *tmp = malloc(sizeof(struct client_thread_data));
        if (tmp == NULL)
        {
            printf("ERROR: Unable to allocate memory for client thread struct\n");
            close_connection(new_clientfd); /* skip serving this client and go to the next */
            continue;
        }

        tmp->clientfd = new_clientfd;
        tmp->thread_id = tid;
        tmp->server_threads = client_threads;

        if ((t_status = pthread_create(&client_threads[tid], NULL, serve_client_thread, tmp)) == 0)
        {
            printf("Successfully created client thread. TID: %d. Array val: %d\n", tid, (int)client_threads[tid]);
            pthread_detach(client_threads[tid]);
        }
        else
        {
            printf("ERROR: Client thread returned with error: %d\n", t_status);
            close_connection(new_clientfd);

            /* join the dead thread and free the array position */
            pthread_join(client_threads[tid], NULL);
            client_threads[tid] = 0;

            free(tmp); /* free the thread struct */
            continue;
        }
    }
    perror("Error accepting client");

    log_write("Main", "POP3 Server shutting down", "", "");
    close_socket(serverfd);

    return 0;
}


void *serve_client_thread(void *thread_data)
{
    /* set thread properties */
    pthread_cleanup_push(thread_cleanup_handler, thread_data);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);


    /* cast the thread parameter back to the struct */
    struct client_thread_data *client_dets = (struct client_thread_data *)thread_data; 

    int current_tid = client_dets->thread_id;
    int clientfd = client_dets->clientfd;

    printf("CLIENT THREAD DETS: clientfd: %d, current_tid: %d\n", clientfd, current_tid);

    /* set the timeout on the client fd */
    struct timeval timeout;
    timeout.tv_sec = 120;
    timeout.tv_usec = 0;

    /*if (setsockopt(clientfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
    {
        printf("ERROR: Unable to set timeout on client socket %d\n", clientfd);
    }*/

    pop_protocol(clientfd); /* serve the client the POP protocol! */

    /* before we exit & cleanup, free this thread's position in the server's thread array */
    client_dets->server_threads[current_tid] = 0;

    pthread_cleanup_pop(1); /* call the thread cleanup function */
    pthread_exit(NULL);

    return NULL; /* return the initial struct so we know which thread has ended? */
}

void thread_cleanup_handler(void *thread_data)
{
    /* the only thing passed to this function will be a thread's client_thread_data struct */
    free(thread_data);
}

void signal_handler(int signum)
{
    printf("\nCaught signal %d\n", signum);
    log_write("Main", "POP3 Server shutting down", "", "");

    cleanup_threads();

    exit(0);
}

void cleanup_threads()
{
    printf("Cleaning up threads\n");
    for (int i = 0; i < MAX_CONCURRENT_CONNECTIONS; i++)
    {
        if (client_threads[i] != 0)
        {
            /* if the thread is still valid, cancel dat shit
            cancelling a thread will make the thread call thread_cleanup_handler */
            pthread_cancel(client_threads[i]);
            client_threads[i] = 0;
        }
    }

}
