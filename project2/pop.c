#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>

#include "pop.h"
#include "socket.h"
#include "maildir.h"
#include "logging.h"



/* ----------------- BEGIN MD5 ------------------- 

All credit for MD5 goes to Aladdin Enterprises @ http://sourceforge.net/projects/libmd5-rfc/ 
*/

#ifndef md5_INCLUDED
#  define md5_INCLUDED

/*
 * This package supports both compile-time and run-time determination of CPU
 * byte order.  If ARCH_IS_BIG_ENDIAN is defined as 0, the code will be
 * compiled to run only on little-endian CPUs; if ARCH_IS_BIG_ENDIAN is
 * defined as non-zero, the code will be compiled to run only on big-endian
 * CPUs; if ARCH_IS_BIG_ENDIAN is not defined, the code will be compiled to
 * run on either big- or little-endian CPUs, but will run slightly less
 * efficiently on either one than if ARCH_IS_BIG_ENDIAN is defined.
 */

typedef unsigned char md5_byte_t; /* 8-bit byte */
typedef unsigned int md5_word_t; /* 32-bit word */

/* Define the state of the MD5 Algorithm. */
typedef struct md5_state_s {
    md5_word_t count[2];    /* message length in bits, lsw first */
    md5_word_t abcd[4];     /* digest buffer */
    md5_byte_t buf[64];     /* accumulate block */
} md5_state_t;

#ifdef __cplusplus
extern "C" 
{
#endif

/* Initialize the algorithm. */
void md5_init(md5_state_t *pms);

/* Append a string to the message. */
void md5_append(md5_state_t *pms, const md5_byte_t *data, int nbytes);

/* Finish the message and return the digest. */
void md5_finish(md5_state_t *pms, md5_byte_t digest[16]);

#ifdef __cplusplus
}  /* end extern "C" */
#endif

#endif /* md5_INCLUDED */

/* ----------------- END MD5 ------------------- */

#define maxUserLen 50
#define maxPassLen 20

enum pop_states
{
    INIT,
    NO_LOGIN,
    AUTHENTICATED_TRANSACTION,
    UPDATE
};

struct user_details
{
    char username[maxUserLen+1];
    char password[maxPassLen+1];

    bool authed;

    FilesStruct *user_files;

    int *deleted_msg_index; /* a pointer to an int array corresponding to messages marked as deleted
                                messages corresponding to an index with value '1' are marked as deleted.
                                '0' means the message is not marked as deleted*/

    time_t epoch_time; /* the time used for APOP auth for this client */

    char apop_timestamp[64]; /* string containing the APOP "timestamp" */
};

char *split_message(char *msg, int delim); /* split the client's message into cmd:arg */
bool is_valid_message(struct user_details *details, int msg_id);
int setup_user_details(struct user_details *cdetails);

/*
* This function is the starting point of the POP protocol
* on the given connection.
*/
void pop_protocol(int clientfd)
{
    /* has anyone really been far even as decided to use even go want to do look more like */
    struct user_details client_details;

    client_details.user_files = NULL;
    client_details.deleted_msg_index = NULL;
    client_details.authed = false;
    memset(client_details.username, 0, maxUserLen+1);
    memset(client_details.password, 0, maxPassLen+1);

    enum pop_states client_state = INIT;

    const char *MSG_OK = "+OK";
    const char *MSG_ERROR = "-ERR";
    const char *MSG_CRLF = "\r\n";
    const char *APOP_SECRET = "thecakeisalie";
    //const char *MSG_DELIM = " ";
    const int MSG_DELIM = 32;

    char *client_message = NULL; /* a pointer to the client message that is returned by socket_get_line() */
    char svr_response[512]; /* maximum length of any message we will send at once */
    memset(svr_response, 0, 512);

    int username_received = 0;

    //char *split_saveptr = NULL;
    char *user_cmd = NULL;
    char *user_arg = NULL;

    if (client_state == INIT)
    {
        client_details.epoch_time = time(NULL);
        sprintf(client_details.apop_timestamp, "<%d.%li@myserver.com>", (int)getpid(), (long)client_details.epoch_time);

        /* construct the initial messages, acknowledging the connection and letting the client know we're ready */
        sprintf(svr_response, "%s HOW IS BABBY FORMED %s%s", MSG_OK, client_details.apop_timestamp, MSG_CRLF);

        socket_write(clientfd, svr_response);
        client_state = NO_LOGIN; /* go to the next state */
    }

    while (true)
    {
        if (client_message != NULL)
        {
            free(client_message);
            client_message = NULL;
        }

        client_message = socket_get_line(clientfd);
        if (client_message == NULL)
        {
            printf("POP: Client disconnected while being served or there was a socket error\n");
            break;
        }

        /* strip the trailing \r\n so we can parse the message nicely */
        int msglen = strlen(client_message);
        if (client_message[msglen-1] == '\n')
            client_message[msglen-1] = 0;
        if (client_message[msglen-2] == '\r')
            client_message[msglen-2] = 0;

        //printf("POP Client fd %d: Received message: %s\n", clientfd, client_message);

        //split_saveptr = NULL;
        user_cmd = NULL;
        user_arg = NULL;

        /* split the string out into command and arguments */
        /*if ((user_cmd = strtok_r(client_message, MSG_DELIM, &split_saveptr)) == NULL)
        {
            printf("POP ERROR: Unable to tokenize message\n");
            continue;
        }
        user_arg = strtok_r(NULL, MSG_DELIM, &split_saveptr);*/

        user_cmd = client_message;
        user_arg = split_message(client_message, MSG_DELIM);

        /* convert the user_cmd to upper case, so we don't need to worry about checking cases and shiz */
        char *p = user_cmd; /* set P to the first char of user_cmd */
        for ( ; *p; ++p) *p = toupper(*p); /* credit to J.F. Sebastian @ stackexchange http://stackoverflow.com/a/2661788 */

        //printf("user command: %s\n", user_cmd);
        //printf("user arg: %s\n", user_arg);

        if (client_state == NO_LOGIN)
        {
            /* wait for the user command, acknowledge, and then wait for the password and acknowledge
            or use APOP */

            if (strcmp(user_cmd, "APOP") == 0)
            {
                log_write("POP", "No Auth", "APOP", user_arg);
                /* apop message: APOP mrose c4c9334bac560ecc979e58001b3e22fb */
                char *client_apop_digest = split_message(user_arg, MSG_DELIM); /* split the user arg into 2 */

                char user_apop_pass[64];
                sprintf(user_apop_pass, "%s%s", client_details.apop_timestamp, APOP_SECRET);
                printf("apop string: %s\n", user_apop_pass);

                /* now we calculate the MD5 hash of the apop pass */
                md5_state_t md5state;
                md5_byte_t digest[16];
                char hex_out[16*2 + 1];

                md5_init(&md5state);
                md5_append(&md5state, (const md5_byte_t *)user_apop_pass, strlen(user_apop_pass));
                md5_finish(&md5state, digest);

                for (int i = 0; i < 16; i++)
                {
                    sprintf(hex_out + i*2, "%02x", digest[i]);
                }

                printf("real MD5: %s, client MD5: %s\n", hex_out, client_apop_digest);

                if (strcmp(hex_out, client_apop_digest) == 0)
                {
                    log_write("POP", user_arg, "Authenticated", "Digest valid");

                    client_details.authed = true;
                    strcpy(client_details.username, user_arg);
                    

                    log_write("POP", client_details.username, "TRANSACTION STATE", "");

                    if (setup_user_details(&client_details) == 1)
                    {
                        printf("ERROR: unable to get user details. hmmm\n");
                        sprintf(svr_response, "%s unable to get user maildir%s", MSG_ERROR, MSG_CRLF);
                        socket_write(clientfd, svr_response);

                        continue;
                    }

                    sprintf(svr_response, "%s launch control transferred to pilot%s", MSG_OK, MSG_CRLF);
                    client_state = AUTHENTICATED_TRANSACTION;
                }
                else
                {
                    sprintf(svr_response, "%s invalid username or password%s", MSG_ERROR, MSG_CRLF);
                }
                socket_write(clientfd, svr_response);
            }
            else if (strcmp(user_cmd, "USER") == 0)
            {
                log_write("POP", "No Auth", "USER", user_arg);
                if (user_arg == NULL)
                {
                    sprintf(svr_response, "%s no user specified%s", MSG_ERROR, MSG_CRLF);
                    socket_write(clientfd, svr_response);
                }
                else
                {
                    strcpy(client_details.username, user_arg);

                    printf("Got username %s\n", client_details.username);
                    username_received = 1;

                    sprintf(svr_response, "%s acknowledged, thunderbirds are go%s", MSG_OK, MSG_CRLF);
                    socket_write(clientfd, svr_response);
                }
            }
            else if (strcmp(user_cmd, "PASS") == 0)
            {
                log_write("POP", "No Auth", "PASS", user_arg);
                if (!username_received)
                {
                    printf("Client is sending password without username\n");
                    sprintf(svr_response, "%s no username specified%s", MSG_ERROR, MSG_CRLF);
                    socket_write(clientfd, svr_response);

                    continue;
                }
                /* check the password */
                if (user_arg == NULL)
                {
                    sprintf(svr_response, "%s no password specified%s", MSG_ERROR, MSG_CRLF);
                    socket_write(clientfd, svr_response);
                }
                else
                {
                    strncpy(client_details.password, user_arg, maxPassLen+1);

                    printf("Got password %s\n", client_details.password);

                    /* now that we have the password, check if the user is valid */
                    bool authed = check_user(client_details.username, client_details.password);

                    if (authed)
                    {
                        /* valid user! */
                        client_details.authed = true;
                        log_write("POP", client_details.username, "Authenticated", "");
                        log_write("POP", client_details.username, "TRANSACTION STATE", "");

                        if (setup_user_details(&client_details) != 0)
                        {
                            printf("ERROR: unable to get user details. hmmm\n");
                            sprintf(svr_response, "%s unable to get user maildir%s", MSG_ERROR, MSG_CRLF);
                            socket_write(clientfd, svr_response);

                            continue;
                        }

                        sprintf(svr_response, "%s access granted!%s", MSG_OK, MSG_CRLF);
                        socket_write(clientfd, svr_response);

                        client_state = AUTHENTICATED_TRANSACTION;
                    }
                    else
                    {
                        log_write("POP", client_details.username, "Auth failed", "");
                        sprintf(svr_response, "%s does not compute, you're under arrest%s", MSG_ERROR, MSG_CRLF);
                        socket_write(clientfd, svr_response);
                    }
                }
            }
            else if (strcmp(user_cmd, "QUIT") == 0)
            {
                /* client issued QUIT in auth state. respond OK then close connection */
                log_write("POP", "No Auth", "QUIT", "");
                sprintf(svr_response, "%s good luck sir%s", MSG_OK, MSG_CRLF);
                socket_write(clientfd, svr_response);

                close_connection(clientfd);
                break;
            }
            else
            {
                printf("Unknown command: %s\n", user_cmd);
                sprintf(svr_response, "%s invalid command%s", MSG_ERROR, MSG_CRLF);
                socket_write(clientfd, svr_response);
            }
        }
        else if (client_state == AUTHENTICATED_TRANSACTION)
        {
            /* now we can serve the transaction shit */
            log_write("POP", client_details.username, user_cmd, user_arg); /* log all commands and args */
            if (strcmp(user_cmd, "STAT") == 0)
            {
                /* STAT - no args
                response format: +OK <num mails> <total size in bytes>\r\n */

                int count = 0;
                int total_file_size = 0;
                /* loop over filesize array to get total file size (of all files) */
                for (int i = 0; i < client_details.user_files->count; i++)
                {
                    if (client_details.deleted_msg_index[i]) continue;

                    total_file_size += client_details.user_files->FileSize[i];
                    count += 1;
                }

                printf("STAT for %s: %d %d\n", client_details.username, count, total_file_size);

                sprintf(svr_response, "%s %d %d%s", MSG_OK, count, total_file_size, MSG_CRLF);
                socket_write(clientfd, svr_response);
            }
            else if (strcmp(user_cmd, "LIST") == 0)
            {
                /* LIST <mail number (optional)>

                cmd: LIST
                arg: null
                    response: +OK
                    response: <msg num> <size> for each message
                    response: . --terminating period with crlf

                arg: <id>
                    response: +OK <id> <size>

                invalid arg:
                    response: -ERR
                        

                if the message is marked as deleted or does not exist, respond with -ERR
                */
                if (user_arg == NULL)
                {
                    /* serve all the msgs.
                    this will respond with a +OK and a period following straight
                    away if there are no messages in the mailbox */

                    sprintf(svr_response, "%s mail listing follows%s", MSG_OK, MSG_CRLF);
                    socket_write(clientfd, svr_response);

                    /* loop for the individual files. don't show files marked as deleted */
                    for (int i = 0; i < client_details.user_files->count; i++)
                    {
                        if (client_details.deleted_msg_index[i]) continue;

                        sprintf(svr_response, "%d %d%s", i+1, client_details.user_files->FileSize[i], MSG_CRLF);
                        socket_write(clientfd, svr_response);
                    }
                    /* send the ending period */
                    sprintf(svr_response, ".%s", MSG_CRLF);
                    socket_write(clientfd, svr_response);
                }
                else
                {
                    /* serve single msg */
                    int msg_int = atoi(user_arg);

                    if (!is_valid_message(&client_details, msg_int))
                    {
                        sprintf(svr_response, "%s invalid message%s", MSG_ERROR, MSG_CRLF);
                    }
                    else
                    {
                        /* valid listing! */
                        sprintf(svr_response, "%s %d %d%s", MSG_OK, msg_int, client_details.user_files->FileSize[msg_int-1], MSG_CRLF);
                    }

                    /* send the response */
                    socket_write(clientfd, svr_response);
                }
            }
            else if (strcmp(user_cmd, "RETR") == 0)
            {
                /* RETR <msg num>
                retrieves data of a message if it exists and is not marked as deleted
                response format:
                +OK
                <msg>
                .

                All '.'s in message are stuffed with a CRLF, which is done by get_file()
                get_file() also appends the terminating '.', so we only need to worry
                about sending here
                */
                if (user_arg == NULL)
                {
                    //printf("RETR has no args\n");
                    sprintf(svr_response, "%s no message supplied%s", MSG_ERROR, MSG_CRLF);
                    socket_write(clientfd, svr_response);
                }
                else
                {
                    //printf("RETR has arg %s", user_arg);
                    int msg_int = atoi(user_arg);

                    if (!is_valid_message(&client_details, msg_int))
                    {
                        sprintf(svr_response, "%s invalid message%s", MSG_ERROR, MSG_CRLF);
                        socket_write(clientfd, svr_response);
                        continue;
                    }

                    /* get the msg data and send it! */
                    char *mail_data = get_file(client_details.username, client_details.user_files->FileNames[msg_int-1]);
                    if (mail_data == NULL)
                    {
                        printf("No data found for msg %d\n", msg_int);
                        sprintf(svr_response, "%s unable to read message%s", MSG_ERROR, MSG_CRLF);
                        socket_write(clientfd, svr_response);

                        /* jump to next read */
                        continue;
                    }
                    /* first we send an +OK */
                    sprintf(svr_response, "%s message is %d bytes%s", MSG_OK, client_details.user_files->FileSize[msg_int-1], MSG_CRLF);
                    socket_write(clientfd, svr_response);

                    /* now we send the message. it is already crlf terminated, and byte stuffed by get_file() */
                    socket_write(clientfd, mail_data);

                    free(mail_data);
                }
            }
            else if (strcmp(user_cmd, "DELE") == 0)
            {
                if (user_arg == NULL)
                {
                    //printf("DELE has no args\n");
                    sprintf(svr_response, "%s no message supplied%s", MSG_ERROR, MSG_CRLF);
                    socket_write(clientfd, svr_response);
                }
                else
                {
                    /* mark the message as deleted in our private client-specific array */
                    int msg_int = atoi(user_arg);

                    if (!is_valid_message(&client_details, msg_int))
                    {
                        sprintf(svr_response, "%s invalid message%s", MSG_ERROR, MSG_CRLF);
                    }
                    else
                    {
                        /* mark message as deleted */
                        client_details.deleted_msg_index[msg_int-1] = 1;

                        sprintf(svr_response, "%s msg %d marked as deleted%s", MSG_OK, msg_int, MSG_CRLF);
                    }
                    socket_write(clientfd, svr_response);
                }
            }
            else if (strcmp(user_cmd, "NOOP") == 0)
            {
                /* no operation, simply respond with +OK */
                sprintf(svr_response, "%s%s", MSG_OK, MSG_CRLF);
                socket_write(clientfd, svr_response);
            }
            else if (strcmp(user_cmd, "RSET") == 0)
            {
                /* unmark any messages that are marked as deleted
                response format: +OK <anything> */
                for (int i = 0; i < client_details.user_files->count; i++)
                {
                    client_details.deleted_msg_index[i] = 0;
                }

                sprintf(svr_response, "%s reset mails marked for deletion%s", MSG_OK, MSG_CRLF);
                socket_write(clientfd, svr_response);
            }
            else if (strcmp(user_cmd, "QUIT") == 0)
            {
                /* enter the update state and break out of this loop
                responses will sent at the completion of the UPDATE state */
                //printf("user quit. entering update state\n");

                client_state = UPDATE;
                break;
            }
            else if (strcmp(user_cmd, "UIDL") == 0)
            {
                /* 
                UIDL responds with unique identifiers for mails in the user's maildir, i.e the filenames
                cmd: UIDL

                arg: null
                    response: +OK uidl follows
                    response: <id> <uid> ...
                    response: .

                arg: <msg>
                    response: +OK <msg> <uid>

                invalid arg:
                    response: -ERR
                */
                if (user_arg == NULL)
                {
                    /* serve all the msgs.
                    this will respond with a +OK and a period following straight
                    away if there are no messages in the mailbox */

                    sprintf(svr_response, "%s uid listing follows%s", MSG_OK, MSG_CRLF);
                    socket_write(clientfd, svr_response);

                    /* loop for the individual files. don't show files marked as deleted */
                    for (int i = 0; i < client_details.user_files->count; i++)
                    {
                        if (client_details.deleted_msg_index[i]) continue;

                        sprintf(svr_response, "%d %s%s", i+1, client_details.user_files->FileNames[i], MSG_CRLF);
                        socket_write(clientfd, svr_response);
                    }
                    /* send the ending period */
                    sprintf(svr_response, ".%s", MSG_CRLF);
                    socket_write(clientfd, svr_response);
                }
                else
                {
                    /* serve single msg */
                    int msg_int = atoi(user_arg);

                    if (!is_valid_message(&client_details, msg_int))
                    {
                        sprintf(svr_response, "%s invalid message%s", MSG_ERROR, MSG_CRLF);
                    }
                    else
                    {
                        /* valid listing! */
                        sprintf(svr_response, "%s %d %s%s", MSG_OK, msg_int, client_details.user_files->FileNames[msg_int-1], MSG_CRLF);
                    }

                    /* send the response */
                    socket_write(clientfd, svr_response);
                }
            }
            else
            {
                printf("Unknown command: %s\n", user_cmd);
                sprintf(svr_response, "%s invalid command%s", MSG_ERROR, MSG_CRLF);
                socket_write(clientfd, svr_response);
            }
        }
        else
        {
            printf("UNKNOWN CLIENT STATE??\n");
        }
    }

    if (client_message != NULL)
        free(client_message);

    /* enter the update state if QUIT was issued during AUTHENTICATED_TRANSACTION */
    if (client_state == UPDATE)
    {
        printf("Updating maildrop for user %s\n", client_details.username);
        log_write("POP", client_details.username, "UPDATE STATE", "");
        /* process updates */
        for (int i = 0; i < client_details.user_files->count; i++)
        {
            if (client_details.deleted_msg_index[i])
            {
                /* file is marked as deleted */

                delete_mail(client_details.username, client_details.user_files->FileNames[i]);
            }
        }

        /* since we don't actually have a return from delete_mail, it can be assumed that we can ignore any errors in it
        therefore, just send a +OK*/
        sprintf(svr_response, "%s maildrop update complete. beam me up scotty%s", MSG_OK, MSG_CRLF);
        socket_write(clientfd, svr_response);
    }

    /* clean up our arrays and what not that have been allocated memory */
    if (client_details.user_files != NULL)
    {
        for (int i = 0; i < client_details.user_files->count; i++)
        {
            if (client_details.user_files->FileNames[i] != NULL)
                free(client_details.user_files->FileNames[i]);
        }
        free(client_details.user_files->FileSize);
        free(client_details.user_files->FileNames);
        free(client_details.user_files);
    }

    if (client_details.deleted_msg_index != NULL)
        free(client_details.deleted_msg_index);

    log_write("POP", client_details.username, "Closing Connection", "");
    printf("Client session over on fd %d. Closing connection\n", clientfd);
    close_connection(clientfd);
}

char *split_message(char *msg, int delim)
{
    /* Takes a string and inserts a null in the first place a character matches 'delim'
    Returns a pointer to the string that comes after the new null

    This allows us to split the message into a command and an arg, essentially replicating
    strtok_r, but keeps the arg intact with any delimeters it contains, which
    is useful for commands that accept arguments with spaces (i.e PASS)
    */

    int tok_position = 0;
    for (int i = 0; i < (int)strlen(msg); i++)
    {
        if (msg[i] == delim)
        {
            /* character matches, so replace it with a NULL */
            msg[i] = '\0';
            tok_position = i+1;
            break;
        }
    }
    if (!tok_position)
        return NULL;
    else
        return msg + tok_position;
}

bool is_valid_message(struct user_details *details, int msg_id)
{
    /* we should make sure the id is within a valid range before calling convert_msg_index

       if the arg is > the msg count, it must be invalid. likewise, if msg_int is < 1, it is invalid
       this catches cases where the argument may not be a number and if a client tries to get message 0 (which would be index -1)
    */
    if (msg_id < 1 || msg_id > details->user_files->count)
    {
        /* message is outside possible range */
        return false;
    }

    if (details->deleted_msg_index[msg_id-1])
        return false;

    return true;
}

int setup_user_details(struct user_details *cdetails)
{
    /* get the user's filestruct for the next states */
    cdetails->user_files = dir_get_list(cdetails->username);
    if (cdetails->user_files == NULL)
    {
        log_write("POP", "Authenticated", "GetList", "Unable to get directory listing");
        return 1;
    }

    cdetails->deleted_msg_index = malloc(sizeof(int) * cdetails->user_files->count);
    if (cdetails->deleted_msg_index == NULL)
    {
        printf("ERROR: Unable to allocate memory for deleted message index\n");
        return 1;
    }

    /* initialise the deleted message index */
    for (int i = 0; i < cdetails->user_files->count; i++)
    {
        cdetails->deleted_msg_index[i] = 0;
    }

    return 0;
}













/* ALL MD5 CODE 
Copyright (C) 1999, 2000, 2002 Aladdin Enterprises.  All rights reserved.
*/
#undef BYTE_ORDER   /* 1 = big-endian, -1 = little-endian, 0 = unknown */
#ifdef ARCH_IS_BIG_ENDIAN
#  define BYTE_ORDER (ARCH_IS_BIG_ENDIAN ? 1 : -1)
#else
#  define BYTE_ORDER 0
#endif

#define T_MASK ((md5_word_t)~0)
#define T1 /* 0xd76aa478 */ (T_MASK ^ 0x28955b87)
#define T2 /* 0xe8c7b756 */ (T_MASK ^ 0x173848a9)
#define T3    0x242070db
#define T4 /* 0xc1bdceee */ (T_MASK ^ 0x3e423111)
#define T5 /* 0xf57c0faf */ (T_MASK ^ 0x0a83f050)
#define T6    0x4787c62a
#define T7 /* 0xa8304613 */ (T_MASK ^ 0x57cfb9ec)
#define T8 /* 0xfd469501 */ (T_MASK ^ 0x02b96afe)
#define T9    0x698098d8
#define T10 /* 0x8b44f7af */ (T_MASK ^ 0x74bb0850)
#define T11 /* 0xffff5bb1 */ (T_MASK ^ 0x0000a44e)
#define T12 /* 0x895cd7be */ (T_MASK ^ 0x76a32841)
#define T13    0x6b901122
#define T14 /* 0xfd987193 */ (T_MASK ^ 0x02678e6c)
#define T15 /* 0xa679438e */ (T_MASK ^ 0x5986bc71)
#define T16    0x49b40821
#define T17 /* 0xf61e2562 */ (T_MASK ^ 0x09e1da9d)
#define T18 /* 0xc040b340 */ (T_MASK ^ 0x3fbf4cbf)
#define T19    0x265e5a51
#define T20 /* 0xe9b6c7aa */ (T_MASK ^ 0x16493855)
#define T21 /* 0xd62f105d */ (T_MASK ^ 0x29d0efa2)
#define T22    0x02441453
#define T23 /* 0xd8a1e681 */ (T_MASK ^ 0x275e197e)
#define T24 /* 0xe7d3fbc8 */ (T_MASK ^ 0x182c0437)
#define T25    0x21e1cde6
#define T26 /* 0xc33707d6 */ (T_MASK ^ 0x3cc8f829)
#define T27 /* 0xf4d50d87 */ (T_MASK ^ 0x0b2af278)
#define T28    0x455a14ed
#define T29 /* 0xa9e3e905 */ (T_MASK ^ 0x561c16fa)
#define T30 /* 0xfcefa3f8 */ (T_MASK ^ 0x03105c07)
#define T31    0x676f02d9
#define T32 /* 0x8d2a4c8a */ (T_MASK ^ 0x72d5b375)
#define T33 /* 0xfffa3942 */ (T_MASK ^ 0x0005c6bd)
#define T34 /* 0x8771f681 */ (T_MASK ^ 0x788e097e)
#define T35    0x6d9d6122
#define T36 /* 0xfde5380c */ (T_MASK ^ 0x021ac7f3)
#define T37 /* 0xa4beea44 */ (T_MASK ^ 0x5b4115bb)
#define T38    0x4bdecfa9
#define T39 /* 0xf6bb4b60 */ (T_MASK ^ 0x0944b49f)
#define T40 /* 0xbebfbc70 */ (T_MASK ^ 0x4140438f)
#define T41    0x289b7ec6
#define T42 /* 0xeaa127fa */ (T_MASK ^ 0x155ed805)
#define T43 /* 0xd4ef3085 */ (T_MASK ^ 0x2b10cf7a)
#define T44    0x04881d05
#define T45 /* 0xd9d4d039 */ (T_MASK ^ 0x262b2fc6)
#define T46 /* 0xe6db99e5 */ (T_MASK ^ 0x1924661a)
#define T47    0x1fa27cf8
#define T48 /* 0xc4ac5665 */ (T_MASK ^ 0x3b53a99a)
#define T49 /* 0xf4292244 */ (T_MASK ^ 0x0bd6ddbb)
#define T50    0x432aff97
#define T51 /* 0xab9423a7 */ (T_MASK ^ 0x546bdc58)
#define T52 /* 0xfc93a039 */ (T_MASK ^ 0x036c5fc6)
#define T53    0x655b59c3
#define T54 /* 0x8f0ccc92 */ (T_MASK ^ 0x70f3336d)
#define T55 /* 0xffeff47d */ (T_MASK ^ 0x00100b82)
#define T56 /* 0x85845dd1 */ (T_MASK ^ 0x7a7ba22e)
#define T57    0x6fa87e4f
#define T58 /* 0xfe2ce6e0 */ (T_MASK ^ 0x01d3191f)
#define T59 /* 0xa3014314 */ (T_MASK ^ 0x5cfebceb)
#define T60    0x4e0811a1
#define T61 /* 0xf7537e82 */ (T_MASK ^ 0x08ac817d)
#define T62 /* 0xbd3af235 */ (T_MASK ^ 0x42c50dca)
#define T63    0x2ad7d2bb
#define T64 /* 0xeb86d391 */ (T_MASK ^ 0x14792c6e)


static void
md5_process(md5_state_t *pms, const md5_byte_t *data /*[64]*/)
{
    md5_word_t
    a = pms->abcd[0], b = pms->abcd[1],
    c = pms->abcd[2], d = pms->abcd[3];
    md5_word_t t;
#if BYTE_ORDER > 0
    /* Define storage only for big-endian CPUs. */
    md5_word_t X[16];
#else
    /* Define storage for little-endian or both types of CPUs. */
    md5_word_t xbuf[16];
    const md5_word_t *X;
#endif

    {
#if BYTE_ORDER == 0
    /*
     * Determine dynamically whether this is a big-endian or
     * little-endian machine, since we can use a more efficient
     * algorithm on the latter.
     */
    static const int w = 1;

    if (*((const md5_byte_t *)&w)) /* dynamic little-endian */
#endif
#if BYTE_ORDER <= 0     /* little-endian */
    {
        /*
         * On little-endian machines, we can process properly aligned
         * data without copying it.
         */
        if (!((data - (const md5_byte_t *)0) & 3)) {
        /* data are properly aligned */
        X = (const md5_word_t *)data;
        } else {
        /* not aligned */
        memcpy(xbuf, data, 64);
        X = xbuf;
        }
    }
#endif
#if BYTE_ORDER == 0
    else            /* dynamic big-endian */
#endif
#if BYTE_ORDER >= 0     /* big-endian */
    {
        /*
         * On big-endian machines, we must arrange the bytes in the
         * right order.
         */
        const md5_byte_t *xp = data;
        int i;

#  if BYTE_ORDER == 0
        X = xbuf;       /* (dynamic only) */
#  else
#    define xbuf X      /* (static only) */
#  endif
        for (i = 0; i < 16; ++i, xp += 4)
        xbuf[i] = xp[0] + (xp[1] << 8) + (xp[2] << 16) + (xp[3] << 24);
    }
#endif
    }

#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32 - (n))))

    /* Round 1. */
    /* Let [abcd k s i] denote the operation
       a = b + ((a + F(b,c,d) + X[k] + T[i]) <<< s). */
#define F(x, y, z) (((x) & (y)) | (~(x) & (z)))
#define SET(a, b, c, d, k, s, Ti)\
  t = a + F(b,c,d) + X[k] + Ti;\
  a = ROTATE_LEFT(t, s) + b
    /* Do the following 16 operations. */
    SET(a, b, c, d,  0,  7,  T1);
    SET(d, a, b, c,  1, 12,  T2);
    SET(c, d, a, b,  2, 17,  T3);
    SET(b, c, d, a,  3, 22,  T4);
    SET(a, b, c, d,  4,  7,  T5);
    SET(d, a, b, c,  5, 12,  T6);
    SET(c, d, a, b,  6, 17,  T7);
    SET(b, c, d, a,  7, 22,  T8);
    SET(a, b, c, d,  8,  7,  T9);
    SET(d, a, b, c,  9, 12, T10);
    SET(c, d, a, b, 10, 17, T11);
    SET(b, c, d, a, 11, 22, T12);
    SET(a, b, c, d, 12,  7, T13);
    SET(d, a, b, c, 13, 12, T14);
    SET(c, d, a, b, 14, 17, T15);
    SET(b, c, d, a, 15, 22, T16);
#undef SET

     /* Round 2. */
     /* Let [abcd k s i] denote the operation
          a = b + ((a + G(b,c,d) + X[k] + T[i]) <<< s). */
#define G(x, y, z) (((x) & (z)) | ((y) & ~(z)))
#define SET(a, b, c, d, k, s, Ti)\
  t = a + G(b,c,d) + X[k] + Ti;\
  a = ROTATE_LEFT(t, s) + b
     /* Do the following 16 operations. */
    SET(a, b, c, d,  1,  5, T17);
    SET(d, a, b, c,  6,  9, T18);
    SET(c, d, a, b, 11, 14, T19);
    SET(b, c, d, a,  0, 20, T20);
    SET(a, b, c, d,  5,  5, T21);
    SET(d, a, b, c, 10,  9, T22);
    SET(c, d, a, b, 15, 14, T23);
    SET(b, c, d, a,  4, 20, T24);
    SET(a, b, c, d,  9,  5, T25);
    SET(d, a, b, c, 14,  9, T26);
    SET(c, d, a, b,  3, 14, T27);
    SET(b, c, d, a,  8, 20, T28);
    SET(a, b, c, d, 13,  5, T29);
    SET(d, a, b, c,  2,  9, T30);
    SET(c, d, a, b,  7, 14, T31);
    SET(b, c, d, a, 12, 20, T32);
#undef SET

     /* Round 3. */
     /* Let [abcd k s t] denote the operation
          a = b + ((a + H(b,c,d) + X[k] + T[i]) <<< s). */
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define SET(a, b, c, d, k, s, Ti)\
  t = a + H(b,c,d) + X[k] + Ti;\
  a = ROTATE_LEFT(t, s) + b
     /* Do the following 16 operations. */
    SET(a, b, c, d,  5,  4, T33);
    SET(d, a, b, c,  8, 11, T34);
    SET(c, d, a, b, 11, 16, T35);
    SET(b, c, d, a, 14, 23, T36);
    SET(a, b, c, d,  1,  4, T37);
    SET(d, a, b, c,  4, 11, T38);
    SET(c, d, a, b,  7, 16, T39);
    SET(b, c, d, a, 10, 23, T40);
    SET(a, b, c, d, 13,  4, T41);
    SET(d, a, b, c,  0, 11, T42);
    SET(c, d, a, b,  3, 16, T43);
    SET(b, c, d, a,  6, 23, T44);
    SET(a, b, c, d,  9,  4, T45);
    SET(d, a, b, c, 12, 11, T46);
    SET(c, d, a, b, 15, 16, T47);
    SET(b, c, d, a,  2, 23, T48);
#undef SET

     /* Round 4. */
     /* Let [abcd k s t] denote the operation
          a = b + ((a + I(b,c,d) + X[k] + T[i]) <<< s). */
#define I(x, y, z) ((y) ^ ((x) | ~(z)))
#define SET(a, b, c, d, k, s, Ti)\
  t = a + I(b,c,d) + X[k] + Ti;\
  a = ROTATE_LEFT(t, s) + b
     /* Do the following 16 operations. */
    SET(a, b, c, d,  0,  6, T49);
    SET(d, a, b, c,  7, 10, T50);
    SET(c, d, a, b, 14, 15, T51);
    SET(b, c, d, a,  5, 21, T52);
    SET(a, b, c, d, 12,  6, T53);
    SET(d, a, b, c,  3, 10, T54);
    SET(c, d, a, b, 10, 15, T55);
    SET(b, c, d, a,  1, 21, T56);
    SET(a, b, c, d,  8,  6, T57);
    SET(d, a, b, c, 15, 10, T58);
    SET(c, d, a, b,  6, 15, T59);
    SET(b, c, d, a, 13, 21, T60);
    SET(a, b, c, d,  4,  6, T61);
    SET(d, a, b, c, 11, 10, T62);
    SET(c, d, a, b,  2, 15, T63);
    SET(b, c, d, a,  9, 21, T64);
#undef SET

     /* Then perform the following additions. (That is increment each
        of the four registers by the value it had before this block
        was started.) */
    pms->abcd[0] += a;
    pms->abcd[1] += b;
    pms->abcd[2] += c;
    pms->abcd[3] += d;
}

void
md5_init(md5_state_t *pms)
{
    pms->count[0] = pms->count[1] = 0;
    pms->abcd[0] = 0x67452301;
    pms->abcd[1] = /*0xefcdab89*/ T_MASK ^ 0x10325476;
    pms->abcd[2] = /*0x98badcfe*/ T_MASK ^ 0x67452301;
    pms->abcd[3] = 0x10325476;
}

void
md5_append(md5_state_t *pms, const md5_byte_t *data, int nbytes)
{
    const md5_byte_t *p = data;
    int left = nbytes;
    int offset = (pms->count[0] >> 3) & 63;
    md5_word_t nbits = (md5_word_t)(nbytes << 3);

    if (nbytes <= 0)
    return;

    /* Update the message length. */
    pms->count[1] += nbytes >> 29;
    pms->count[0] += nbits;
    if (pms->count[0] < nbits)
    pms->count[1]++;

    /* Process an initial partial block. */
    if (offset) {
    int copy = (offset + nbytes > 64 ? 64 - offset : nbytes);

    memcpy(pms->buf + offset, p, copy);
    if (offset + copy < 64)
        return;
    p += copy;
    left -= copy;
    md5_process(pms, pms->buf);
    }

    /* Process full blocks. */
    for (; left >= 64; p += 64, left -= 64)
    md5_process(pms, p);

    /* Process a final partial block. */
    if (left)
    memcpy(pms->buf, p, left);
}

void
md5_finish(md5_state_t *pms, md5_byte_t digest[16])
{
    static const md5_byte_t pad[64] = {
    0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    md5_byte_t data[8];
    int i;

    /* Save the length before padding. */
    for (i = 0; i < 8; ++i)
    data[i] = (md5_byte_t)(pms->count[i >> 2] >> ((i & 3) << 3));
    /* Pad to 56 bytes mod 64. */
    md5_append(pms, pad, ((55 - (pms->count[0] >> 3)) & 63) + 1);
    /* Append the length. */
    md5_append(pms, data, 8);
    for (i = 0; i < 16; ++i)
    digest[i] = (md5_byte_t)(pms->abcd[i >> 2] >> ((i & 3) << 3));
}
