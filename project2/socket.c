#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "socket.h"
/*
* Creates a new socket and returns the socket descriptor.
*/
int socket_setup(int portno)
{
    int serverfd;

    /* prepare socket */
	struct sockaddr_in srv_addr;
    memset(&srv_addr, 0, sizeof(srv_addr));

	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = INADDR_ANY;
	srv_addr.sin_port = htons(portno);

    serverfd = socket(AF_INET, SOCK_STREAM, 0);

    /* let the server reuse an address. will not use an address that is being used with the same port */
    int reuse_opt = 1;
    setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &reuse_opt, sizeof(int));

    /* bind the server socket */
    if (bind(serverfd, (struct sockaddr *) &srv_addr, sizeof(srv_addr)) == -1)
    {
        printf("ERROR BINDING TO PORT %d\n", portno);
        close(serverfd);
        exit(2);
    }

    /* start listening */
    if (listen(serverfd, 10) == -1)
    {
        printf("ERROR LISTENING\n");
        close(serverfd);
        exit(3);
    }

    printf("POP3 Server listening on 0.0.0.0:%d\n", portno);

	return serverfd;
}

/*
* Listens for a new connection,
* once there is a new connection
* the file descriptor of the new open connection is returned.
*/
int socket_get_new_connection(int serverfd)
{
    struct sockaddr_in client_addr;
    socklen_t clilen = sizeof(client_addr);

    //block until there is something to return
    int clientfd = accept(serverfd, (struct sockaddr*) &client_addr, &clilen);

    printf("Client accepted on FD %d\n", clientfd);
	return clientfd;
}

/*
* Reads a 'line' from the given connection.
* A 'line' means a sequence of characters ending with a CRLF pair.
* The string being returned must include the CRLF pair.
*/
char *socket_get_line(int fd)
{
    /* we'll read from the buffer continously until a CRLF (\r\n) is read, then we'll pop the line
    only one line is ever returned at a time

    recv blocks until there is data available to be read
    */
    int buf_size = 32, rtn_size, buf_to_read = 0;

    char *buffer = (char *)malloc(buf_size);
    char *newbuffer;
    char *rtnbuffer;

    if (buffer == NULL)
    {
        printf("ERROR ALLOCATING BUFFER MEMORY FOR CLIENT %d\n", fd);
        exit(EXIT_FAILURE);
    }

    /* loop over the buffer, peeking at the data until we have \r\n */
    while ((rtn_size = recv(fd, buffer, buf_size, MSG_PEEK)) > 0)
    {
        for (int i = 1; i < rtn_size; i++)
        {
            /* \r is 13, \n is 10 */
            if (buffer[i-1] == 13 && buffer[i] == 10)
            {
                /*this is a CRLF. now we want to read the buffer up to this point, leaving the rest in for the next new line
                The number of bytes to read from the buffer is the position of \n + 1, i.e, i+1. This is because i starts at 0,
                but we want a numerical count of the number of bytes we want to read.
                */
                buf_to_read = i + 1;
                //printf("Have CRLF. CRLF at %d,%d \nNum to read: %d.\n Data in buffer:\n", i-1, i, buf_to_read);

                break;
            }
        }

        /* if the rtn_size is the same as the buffer size, we need to increase the buffer size */
        if (rtn_size == buf_size)
        {
            buf_size += 32; //increase the buffer size
            newbuffer = realloc(buffer, buf_size);

            if (newbuffer == NULL)
            {
                printf("OUT OF MEMORY OR UNABLE TO REALLOC THE BUFFER FOR CLIENT %d\n", fd);
                free(buffer);
                return NULL;
            }

            buffer = newbuffer;
            newbuffer = NULL;

            //printf("buffer size increased. new buffer size: %d\n currently in buffer: %s\n", buf_size, buffer);
        }

        if (buf_to_read) break;
    }
    free(buffer);

    if (rtn_size == -1)
    {
        perror("An error occurred during recv");
        return NULL;
    }
    if (rtn_size == 0)
    {
        /* connection was closed */
        return NULL;
    }

    if (buf_to_read)
    {
        rtnbuffer = malloc(sizeof(char) * (buf_to_read + 1));
        /* read up to buf_to_read from the connection */
        //int num_read = 
        read(fd, rtnbuffer, buf_to_read);

        //printf("num read: %d, expected: %d\n", num_read, buf_to_read);

        //printf("changing char at position %d in rtnbuffer to 0\n", buf_to_read);
        rtnbuffer[buf_to_read] = 0;

        /*printf("Chars read from buffer:\n");
        for (int i = 0; i < num_read+1; i++)
        {
            printf("C %d: %d\n", i, rtnbuffer[i]);
        }
        */
    }


    //printf("Received line: %s\n", buffer);
	return rtnbuffer;
}


/*
* Writes the given string to the given connection descriptor.
*/
void socket_write(int fd, char* string)
{
    if (fd == -1)
    {
        /* client disconnected; ABORT! */
        return;
    }

    int rtn_size, bytes_sent = 0, string_len = strlen(string);

    /* all of the data may not be sent instantly. Therefore, we need to loop until it is all sent */
    while (bytes_sent < string_len)
    {
        /* send as much as possible that hasn't been sent. the remaining bytes to be sent is string_len - bytes_sent
        the bytes_sent index will send all bytes from that position until the end of the string (i.e a null terminator) */
        rtn_size = send(fd, string + bytes_sent, string_len - bytes_sent, 0);

        if (rtn_size == -1)
        {
            printf("ERROR SENDING DATA\n");
            return;
        }

        bytes_sent += rtn_size;
    }

    //printf("msg sent\n");
}


/*
* Closes the given connection descriptor.
*/
void close_connection(int fd)
{
    printf("Closing connection on fd %d\n", fd);
    close(fd);
}

/*
* Closes the given socket.
*/
void close_socket(int fd)
{
    printf("Closing listening socket\n");
    close(fd);
}

