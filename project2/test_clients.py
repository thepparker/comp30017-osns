import poplib
import threading
import time

class pop3_test(object):
    def __init__(self):
        
        self.clients = set()

    def add_client(self, user, password):
        new_client = poplib.POP3("192.168.35.128", 12345, 10)
        new_client.set_debuglevel(True)
        self.clients.add(new_client)

        # auth and shiz
        new_client.user_name = user
        #new_client.user(user)
        #new_client.pass_(password)
        new_client.apop(user, "thecakeisalie")

    def do_tests(self):
        #start a new thread for each client

        for client in self.clients: #dont need shallow copy of set because nothing will be done to it
            thread = threading.Thread(target = self.client_thread, args=(client,))
            thread.daemon = True
            thread.start()

    def client_thread(self, client):

        print "%s: %s" % (client.user_name, client.getwelcome())
        print "%s: %s" % (client.user_name, client.stat())
        print "%s: %s" % (client.user_name, client.noop())
        for msg in range(len(client.list()[1])):
            print "%s: %s" % (client.user_name, client.noop())
            print "Getting message id %s" % (msg+1)
            print "%s: %s" % (client.user_name, client.retr(msg+1)[1])

        for msg in range(len(client.list()[1])):
            print "%s: %s" % (client.user_name, client.dele(msg+1))
            
        #print "%s: %s" % (client.user_name, client.rset())
        

        print "%s: %s" % (client.user_name, client.quit())

if __name__ == '__main__':
    test_obj = pop3_test()


    test_obj.add_client("user1", "qwerty")
    test_obj.add_client("user2", "password1")

    test_obj.do_tests()

        